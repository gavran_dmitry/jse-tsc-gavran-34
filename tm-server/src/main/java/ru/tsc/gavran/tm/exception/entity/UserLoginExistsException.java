package ru.tsc.gavran.tm.exception.entity;

import ru.tsc.gavran.tm.exception.AbstractException;

public class UserLoginExistsException extends AbstractException {

    public UserLoginExistsException(String login) {
        super("Error! User with this login '" + login + "' already exist.");
    }

}