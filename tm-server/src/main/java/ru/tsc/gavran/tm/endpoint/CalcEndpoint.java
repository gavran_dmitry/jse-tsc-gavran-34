package ru.tsc.gavran.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class CalcEndpoint {

    @WebMethod
    public int sum(
            @WebParam(name = "a") Integer a,
            @WebParam(name = "b") Integer b
    ){
        return a + b;
    }

}
