package ru.tsc.gavran.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.gavran.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.gavran.tm.api.repository.*;
import ru.tsc.gavran.tm.api.service.*;
import ru.tsc.gavran.tm.endpoint.*;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.exception.system.UnknownCommandException;
import ru.tsc.gavran.tm.model.Project;
import ru.tsc.gavran.tm.model.Session;
import ru.tsc.gavran.tm.repository.*;
import ru.tsc.gavran.tm.service.*;
import ru.tsc.gavran.tm.util.SystemUtil;
import ru.tsc.gavran.tm.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final IServiceLocator serviceLocator = this;

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService, propertyService);

    @NotNull
    private final LogScanner logScanner = new LogScanner();

    @NotNull
    private final Backup backup = new Backup(this, propertyService, serviceLocator);

    @NotNull
    private final CalcEndpoint calcEndpoint = new CalcEndpoint();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, serviceLocator);

    @NotNull
    private final IDataService dataService = new DataService(serviceLocator);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(serviceLocator);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(serviceLocator, backup);

    public void start(String[] args) {
        logScanner.init();
        initUsers();
        initData();
        initEndpoint();
        initPID();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }



    private void initEndpoint() {
       registry(sessionEndpoint);
       registry(userEndpoint);
       registry(adminUserEndpoint);
       registry(adminDataEndpoint);
       registry(projectEndpoint);
       registry(taskEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        final int port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + "/" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void initUsers() {
        userService.create("Test", "Test", Role.USER);
        userService.updateUserByLogin("Test", "LTest1", "FTest", "MTest", "test@gmail.com");
        userService.create("Admin", "Admin", Role.ADMIN);
        userService.updateUserByLogin("Admin", "GAVRAN", "Dmitry", "Mihailovich", "gavran@gmail.com");
    }

    private void initData() {
        projectService.create(userRepository.findByLogin("Test").getId(), "Project C", "1");
        projectService.create(userRepository.findByLogin("Test").getId(), "Project A", "2");
        projectService.create(userRepository.findByLogin("Admin").getId(), "Project B", "3");
        projectService.create(userRepository.findByLogin("Admin").getId(), "Project D", "4");
        taskService.create(userRepository.findByLogin("Test").getId(), "Task C", "1");
        taskService.create(userRepository.findByLogin("Test").getId(), "Task A", "2");
        taskService.create(userRepository.findByLogin("Admin").getId(), "Task B", "3");
        taskService.create(userRepository.findByLogin("Admin").getId(), "Task D", "4");
        projectService.finishByName(userRepository.findByLogin("Test").getId(), "Project C");
        projectService.startByName(userRepository.findByLogin("Admin").getId(), "Project D");
        taskService.finishByName(userRepository.findByLogin("Test").getId(), "Task C");
        taskService.startByName(userRepository.findByLogin("Admin").getId(), "Task B");
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return dataService;
    }

}