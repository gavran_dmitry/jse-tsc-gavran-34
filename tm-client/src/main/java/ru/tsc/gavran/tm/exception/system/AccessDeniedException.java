package ru.tsc.gavran.tm.exception.system;

import ru.tsc.gavran.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! You are denied access to this manager.");
    }

}